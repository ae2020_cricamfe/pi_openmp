# AE2020: Práctica cálculo pi con OpenMP 
#### Cristian Campos Ferrer
[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)
### Indice
- ***~/cpp*** : contiene todos los archivos .cpp
- ***~/exe*** : contiene los ejecutables, secuencial y paralelo
- ***~/output*** : contiene las salidas e imágenes necesarias para el estudio de escalabilidad
- ***~/pi-openmp*** : parte entregable de la práctica de pi con OpenMP
- ***~/python*** : contine los scripts utilizados para el estudio de escalabilidad
- ***./run.py*** : script principal en python.


### Instalación
```sh
$ git clone https://cricamfe@bitbucket.org/ae2020_cricamfe/pi_openmp.git
```
