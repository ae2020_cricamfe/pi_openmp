import os
import sys

piseq = 'pi.cpp'
pipara = 'pi_openmp.cpp'
salida = sys.argv[1]

compilarPiSeq = 'g++ -o exe/piseq cpp/'+piseq+' -std=c++11 -O3'
compilarPiPara = 'g++ -o exe/pipara cpp/'+pipara+' -std=c++11 -fopenmp -O3'

#Pi secuencial
os.system(compilarPiSeq)
#Pi paralelo
os.system(compilarPiPara)

#Creamos carpeta para guardar salida e imagenes
if not os.path.exists('output/'+ salida):
    os.mkdir('output/'+ salida)

#Ejecutamos launch.py
os.system('python3 python/launch.py | tee output/' + salida + '/' + salida)
#Ejecutamos procesa.py
os.system('python3 python/procesa.py output/' + salida + '/' + salida + ' '+salida)
