import os
import multiprocessing
import subprocess
import sys

#Vemos la cantidad de cores lógicos que tenemos en el procesador
totalCores = multiprocessing.cpu_count()

#sizes = [1000000, 100000000]
sizes = [1000000, 100000000, 10000000000, 100000000000]

numcores = []

n=2
while (n <= totalCores):
    numcores.append(n)
    n=n*2

for s in sizes:
    os.system("echo ")
    os.system("echo Problem size: %d " % (s))
    os.system("./exe/piseq %d " % (s))
    for nc in numcores:
        os.system("./exe/pipara %d %d" % (s, nc))
