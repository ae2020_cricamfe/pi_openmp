#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import numpy as np
import matplotlib.pyplot as plt
import sys
import re

keys = set(['Time:','size:'])

values = []
threads = []
sp = []
sizes = []
ef =[]

with open(sys.argv[1]) as f:
    for line in f:
        tokens = line.split()
        if len(tokens) and tokens[0] in keys:
            values.append(tokens[1])
            threads.append(int(tokens[4]))
        if len(tokens) and tokens[1] in keys:
            sizes.append(tokens[2])

j=0
print('')
for (v,x) in zip(values,threads):
  if int(x) == 1:
    j=j+1
    ser=float(v)
  print ("Cores: %s \t Sec: %s    \tSpeed-up: %s" % (x, v, ser/float(v)))
  spd=ser/float(v)
  sp.append(spd)
  ef.append(spd/int(x))

puntos=int(len(sp)/j)
print("")
print ("Número de experimentos a pintar: %d " % j)
print ("Número de puntos en cada experimento: %d" % puntos)


#Configuration variables
titlefs = 20
ylabelfs = 12
xlabelfs = 12
xticksfs = 16
yticksfs = 16
legendfs = 14
linew = 2
markers = 8

os.chdir('output/'+sys.argv[2])
if not os.path.exists('img'):
    os.mkdir('img')
os.chdir('img')

fig = plt.figure()

marks=['o-','x-','+-','v-','s-']

for i in range(j):
    init=i*puntos
    end= (i+1)*puntos
    plt.plot(np.array(threads[init:end]), np.array(sp[init:end]), marks[i], linewidth=linew, markersize=markers)

plt.plot(np.array(threads[0:puntos]), np.array(threads[0:puntos]), '-', linewidth=linew, markersize=markers)

sizes.append('Ideal')

plt.title('Speed-Up ' + sys.argv[2],  fontweight='bold', fontsize=titlefs)
plt.legend(sizes,loc='best', fontsize= legendfs)
plt.ylabel('Speed-Up', fontsize=ylabelfs)
plt.xlabel('numThreads', fontsize=xlabelfs)
plt.xticks(fontsize=xticksfs)
plt.yticks(fontsize=yticksfs, )
plt.grid()

fig.savefig('Speed-Up ' + sys.argv[2] + ".png")

fig2 = plt.figure()
for i in range(j):
    init=i*puntos
    end= (i+1)*puntos
    plt.plot(np.array(threads[init:end]), np.array(ef[init:end]), marks[i], linewidth=linew, markersize=markers)

plt.plot(np.array(threads[0:puntos]), np.ones(puntos), '-', linewidth=linew, markersize=markers)

plt.title('Efficiency ' + sys.argv[2],  fontweight='bold', fontsize=titlefs)
plt.legend(sizes,loc='best', fontsize= legendfs)
plt.ylabel('Speed up', fontsize=ylabelfs)
plt.xlabel('numThreads', fontsize=xlabelfs)
plt.xticks(fontsize=xticksfs)
plt.yticks(fontsize=yticksfs, )
plt.grid()

fig2.savefig('Efficiency ' +sys.argv[2] + '.png')

plt.show()
