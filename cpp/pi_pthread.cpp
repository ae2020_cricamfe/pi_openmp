//Nota: no olvides compilar con -O3
#include <thread>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <chrono>

using namespace std;

//Nota: usa el menor número posible de variables globales
double *sum;

//Nota: pasa los argumentos que necesites al thread
void mypi(int me, long N, int Nthreads) {
  double x, h, acc=0;

  h   = 1.0 / (double) N;
//Nota: ojo con el rango (long i) y el particionado (+1 y <=)
  for (long i = (N*me)/Nthreads+1; i <= (N*(me+1))/Nthreads; i ++) {
    x = h * ((double)i - 0.5);
    acc += 4.0 / (1.0 + x*x);
    //Nota: no hagas esto o tendrás false sharing:
    //sum[me] += 4.0 / (1.0 + x*x);
  }
  sum[me] = acc*h;
}


int main(int argc, char *argv[])
{
  constexpr double PI25DT = 3.141592653589793238462643;
//Nota: usa atol para convertir ascii a long
  long int N = (argc > 1) ? atol(argv[1]) : 1000;
  int Nthreads = (argc > 2) ? atoi(argv[2]) : 1;

  sum = new double [Nthreads];
  vector<thread> threads;
  auto time1=chrono::high_resolution_clock::now();

//Nota: es mejor usar {} que () para llamar a un constructor
  for (int i=0; i<Nthreads; i++)
    threads.push_back(thread{mypi, i, N, Nthreads});

  for (auto &t: threads)
    t.join();

//Nota: reducción serie. Para este problema una versión paralela no mejora
  for (int i=1; i<Nthreads; i++)
    sum[0] += sum[i];

  auto time2=chrono::high_resolution_clock::now();
  double elapsed = chrono::duration<double,milli>(time2-time1).count();

  printf("pi is approximately %.16f, Error is %.16f\n", sum[0], fabs(sum[0] - PI25DT));
  printf("Time: %f ms, with: %d threads\n",elapsed,Nthreads);
  return 0;
}
