//Nota: no olvides compilar con -O3
#include <thread>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <vector>
#include <chrono>
#include <atomic>

using namespace std;

//Nota: usa el menor número posible de variables globales
atomic<double> sum{0}; //Una buena forma de inicializar sum

//Nota: pasa los argumentos que necesites al thread
void mypi(int me, long N, int Nthreads) {
  double x, h, acc=0;

  h   = 1.0 / (double) N;
//Nota: ojo con el rango (long i) y el particionado (+1 y <=)
  for (long i = (N*me)/Nthreads+1; i <= (N*(me+1))/Nthreads; i ++) {
    x = h * ((double)i - 0.5);
    acc += 4.0 / (1.0 + x*x);
    //Nota: no hagas esto o tendrás muchos conflictos y true sharing:
    //sum += 4.0 / (1.0 + x*x);
  }
  sum = sum + acc*h;
}


int main(int argc, char *argv[])
{
  constexpr double PI25DT = 3.141592653589793238462643;
//Nota: usa atol para convertir ascii a long
  long int N = (argc > 1) ? atol(argv[1]) : 1000;
  int Nthreads = (argc > 2) ? atoi(argv[2]) : 1;

  vector<thread> threads;
  auto time1=chrono::high_resolution_clock::now();

//Nota: es mejor usar {} que () para llamar a un constructor
  for (int i=0; i<Nthreads; i++)
    threads.push_back(thread{mypi, i, N, Nthreads});

  for (auto &t: threads)
    t.join();

  auto time2=chrono::high_resolution_clock::now();
  double elapsed = chrono::duration<double,milli>(time2-time1).count();

  printf("pi is approximately %.16f, Error is %.16f\n", sum.load(), fabs(sum - PI25DT));
  printf("Time: %f ms, with: %d threads\n",elapsed,Nthreads);
  return 0;
}
