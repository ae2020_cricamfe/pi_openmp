#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <chrono>
#include <omp.h>

using namespace std;

int main(int argc, char *argv[])
{
  constexpr double PI25DT = 3.141592653589793238462643;
  double sum;

  if (argc != 3) {
    printf("Usa: %s N num_threads\n",argv[0]);
    exit(0);
  }

  long int n = (argc > 1) ? atol(argv[1]) : 1000;
  int Nthreads = (argc > 2) ? atoi(argv[2]) : 1;

  auto time1=chrono::high_resolution_clock::now();

  sum = 0.0;

  #pragma omp parallel shared(n,sum) num_threads(Nthreads)
  {
    double h  = 1.0 / (double) n;
    double x, acc = 0.0;

    #pragma omp for nowait
      for (long int i = 1; i <= n; i ++){
        x = h * ((double)i - 0.5);
        acc += 4.0 / (1.0 + x*x);
      }

    #pragma omp reduction(+:sum)
    {
      sum += acc*h;
    }
  }

  auto time2=chrono::high_resolution_clock::now();
  double elapsed = chrono::duration<double,milli>(time2-time1).count();

  printf("pi es aproximadamente: %.16f, el error es: %.16f\n", sum, fabs(sum - PI25DT));
  printf("Time: %f ms, with: %d threads\n",elapsed,Nthreads);
  return 0;
}
