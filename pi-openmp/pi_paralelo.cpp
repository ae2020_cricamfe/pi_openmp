#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <omp.h>

int main(int argc, char *argv[])
{
  constexpr double PI25DT = 3.141592653589793238462643;
  double mypi, h, sum, x, elapsed, time1, time2;

  if (argc != 3) {
    printf("Usa: %s N num_threads\n",argv[0]);
    exit(0);
  }

  long int n = (argc > 1) ? atol(argv[1]) : 1000;
  int Nthreads = (argc > 2) ? atoi(argv[2]) : 1;

  h   = 1.0 / (double) n;
  sum = 0.0;

  time1 = omp_get_wtime()*1000;

#pragma omp parallel for private(x) shared(n,h) reduction(+:sum) \
num_threads(Nthreads)
  for (long int i = 1; i <= n; i ++) {
    x = h * ((double)i - 0.5);
    sum += 4.0 / (1.0 + x*x);
  }

  mypi = h * sum;

  time2 = omp_get_wtime()*1000;

  elapsed = time2-time1;

  printf("pi es aproximadamente: %.16f, el error es: %.16f\n", mypi, fabs(mypi - PI25DT));
  printf("Time: %f ms, with: %d threads\n", elapsed, Nthreads);

  return 0;
}
