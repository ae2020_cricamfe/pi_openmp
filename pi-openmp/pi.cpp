#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <chrono>

using namespace std;

int main(int argc, char *argv[])
{
  long int n, i;
  constexpr double PI25DT = 3.141592653589793238462643;
  double mypi, h, sum, x;

  if (argc != 2) {
    printf("Usa: %s N\n",argv[0]);
    exit(0);
  }
  //Nota: usa atol para convertir ascii a long
  n = atol(argv[1]);

  auto time1=chrono::high_resolution_clock::now();
  h   = 1.0 / (double) n;
  sum = 0.0;

  for (i = 1; i <= n; i ++) {
    x = h * ((double)i - 0.5);
    sum += 4.0 / (1.0 + x*x);
  }

  mypi = h * sum;
  auto time2=chrono::high_resolution_clock::now();
  double elapsed = chrono::duration<double,milli>(time2-time1).count();

  printf("pi es aproximadamente: %.16f, el error es: %.16f\n", mypi, fabs(mypi - PI25DT));
  printf("Time: %f ms, with: 1 thread\n",elapsed);
  return 0;
}
